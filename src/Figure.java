import java.awt.*;

/**
 * Created by Mikio on 2014/07/22.
 */
public interface Figure {
    public void draw (Graphics g);
}
