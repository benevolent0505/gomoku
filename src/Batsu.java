import java.awt.*;

/**
 * Created by Mikio on 2014/07/23.
 */
public class Batsu extends SimpleFigure{
    private int size;

    public Batsu(int x, int y, int size) {
        super(x, y);
        this.size = size;
    }

    public void draw (Graphics g) {
        super.draw(g);
        g.drawLine(xPos-size, yPos-size, xPos+size, yPos+size);
        g.drawLine(xPos-size, yPos+size, xPos+size, yPos-size);
    }
}
