import java.awt.*;

/**
 * Created by Mikio on 2014/07/23.
 */
public class Maru extends SimpleFigure {
    private int size;

    public Maru(int x, int y, int size) {
        super(x, y);
        this.size = size;
    }

    public void draw (Graphics g) {
        super.draw(g);
        g.drawOval(xPos-size, yPos-size, 2*size, 2*size);
    }
}
