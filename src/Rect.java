import java.awt.*;

/**
 * Created by Mikio on 2014/07/23.
 */
public class Rect extends SimpleFigure{
    private Color color;
    private int width, height;

    public Rect(Color color, int x, int y, int w, int h) {
        super(x, y);
        this.color = color;
        this.width = w;
        this.height = h;
    }

    public boolean isHit (int x, int y) {
        return xPos - width / 2 <= x && x <= xPos + width / 2 && yPos - height / 2 <= y && y <= yPos + height / 2;
    }

    public int getX () {
        return xPos;
    }

    public int getY () {
        return yPos;
    }

    public void draw (Graphics g) {
        g.setColor(color);
        g.fillRect(xPos-width/2, yPos-height/2, width, height);
    }
}
