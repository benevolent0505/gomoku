import java.awt.*;

/**
 * Created by Mikio on 2014/07/22.
 */
public abstract class SimpleFigure implements Figure{
    public int xPos, yPos;

    public SimpleFigure (int x, int y) {
        this.xPos = x;
        this.yPos = y;
    }

    public void draw (Graphics g) {
        g.setColor(Color.black);
        ((Graphics2D)g).setStroke(new BasicStroke());
    }
}
