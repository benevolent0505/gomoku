import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

/**
 * Created by Mikio on 2014/07/22.
 */
public class MainPanel extends JPanel implements MouseListener{
    private static final int WIDTH = 640;
    private static final int HEIGHT = 480;

    private ArrayList<Figure> figures = new ArrayList<Figure>();
    private boolean turn;

    public MainPanel () {
        setPreferredSize(new Dimension(WIDTH, HEIGHT));

        for (int i = 0; i < 25; ++i) {
            figures.add(new Rect(Color.pink, 80+(i/5)*60, 40+(i%5)*60, 56, 56));
        }
        figures.add(new Batsu(450, 40, 24));

        turn = true;

        addMouseListener(this);
    }

    public void paintComponent(Graphics g){
        super.paintComponent(g);
        for (Figure f : figures) {
            f.draw(g);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        Rect rect = pick(e.getX(), e.getY());
        if (rect == null) {
            return;
        }
        figures.remove(figures.size() - 1);
        if (turn) {
            figures.add(new Batsu(rect.getX(), rect.getY(), 24));
            figures.add(new Maru(450, 40, 24));
        } else {
            figures.add(new Maru(rect.getX(), rect.getY(), 24));
            figures.add(new Batsu(450, 40, 24));
        }
        turn = !turn;
        repaint();
    }

    private Rect pick(int x, int y) {
        Rect rect = null;
        for (Figure f : figures) {
            if (f instanceof Rect && ((Rect)f).isHit(x, y)) {
                rect = (Rect)f;
            }
        }
        return rect;
    }

    @Override
    public void mousePressed(MouseEvent e) {}
    @Override
    public void mouseReleased(MouseEvent e) {}
    @Override
    public void mouseEntered(MouseEvent e) {}
    @Override
    public void mouseExited(MouseEvent e) {}

    /*
    (1)ます目を増やして、五目並べができるようにしよう。
    (2)同じ場所に重ねて打てないようにしよう。
    (3)勝負の判定ができるようにしよう。
     */
}
