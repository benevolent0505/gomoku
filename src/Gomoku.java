import javax.swing.*;
import java.awt.*;

/**
 * Created by Mikio on 2014/07/22.
 */
public class Gomoku extends JFrame {
    public Gomoku () {
        setTitle("五目並べ");

        MainPanel panel = new MainPanel();
        Container contentPane = getContentPane();
        contentPane.add(panel);

        pack();
    }

    public static void main(String[] args) {
        Gomoku frame = new Gomoku();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
    }
}
